import { useState, useEffect } from 'react';

//import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	//checks to see if the mocj data was captured
	//console.log(coursesData);
	//console.log(coursesData[0]);
	
	//State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	// Retrieves the courses from the database upon initial render of the "Courses component"
	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/courses/allActiveCourses`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return (<CourseCard key={course.id} courseProps ={course} />)
			}))
		})

	},[])


	/*const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProps ={course} />
			)
	})*/

	return(
		<>
		{courses}
			{/* 

			{} - used in props to signify that we are providing information
			
			Props Drilling - we can pass information from one component to another using props

			*/}
			{/*<CourseCard courseProp ={coursesData[0]} />
			*/}
		</>
	)
}