import { useState, useEffect, useContext } from 'react';

import { Form, Button } from 'react-bootstrap';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Login() {

    //useContext() - Allows us to use the UserContext object and its properties to be used for user validation.
    const {user, setUser} = useContext(UserContext);

    //State hooks to store values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
   	const [isActive, setIsActive] = useState(false);

    //hook returns a function that lets you navigate to components
    //const navigate = useNavigate();

   	function authenticate(e){
   		e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            //if no user information is found, the "access" property will not be available and returns undefined

            if (typeof data.access !== "undefined") {
                //The JWT will be used to retrieve user information across the whole frontend application and storing it in the localStorage
                localStorage.setItem('token', data.access)

                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })

            } else {
                Swal.fire({
                    title: "Authentication Failed",
                    icon: "error",
                    text: "Please, check your login details and try again."
                })
            }
        });


        // set the email of the authenticated user in local storage
        // localStorage.setItem('propertyName', value)
        //localStorage.setItem('email', email);
        //Sets the global user state to have properties obtained from local storage
        //setUser({email: localStorage.getItem('email')});

   		setEmail('');
        setPassword('');
        // navigate('/');

      //  alert('You are now logged in!')
   	}

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            // get method is default, so it is unnecessary to put
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Global user state for validation accross the whole app
            //changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across whole application
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }


   	useEffect(()=> {
        if(email !== '' && password !== ''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [email, password]);

	return (
		
        (user.id !== null) ?
        <Navigate to="/courses"/>
        :
        <Form onSubmit={(e) => authenticate(e)}>
        <h1>Login</h1><br/>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value = {email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value = {password}
                    onChange = {e => setPassword(e.target.value)}
                    required
                />
            </Form.Group><br/>

            {isActive ?
            <Button variant="success" type="submit" id="submitBtn">
                Submit 
            </Button>
            :
            <Button variant="success" type="submit" id="submitBtn" disabled>
                Submit 
            </Button>
            }
        </Form>
    )

}